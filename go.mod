module mine-go

go 1.17

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/fasthttp/websocket v1.4.3-beta.1 // indirect
	github.com/gofiber/fiber v1.13.3 // indirect
	github.com/gofiber/fiber/v2 v2.27.0 // indirect
	github.com/gofiber/utils v0.0.9 // indirect
	github.com/gofiber/websocket v0.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/schema v1.1.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/klauspost/compress v1.14.1 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/savsgio/gotils v0.0.0-20200616100644-13ff1fd2c28c // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.33.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220111092808-5a964db01320 // indirect
)
