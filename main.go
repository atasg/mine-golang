package main

import (
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"fmt"
)

type MinePosition struct{
	Left int `json:"left"`
	Top int `json:"top"`
}

type InitialMessage struct{
	Start bool `json:"start"`
	Restart bool `json:"restart"`
}

var (
	wsUpgrader = websocket.Upgrader {
		ReadBufferSize : 1024,
		WriteBufferSize : 1024,
	}

	wsConn *websocket.Conn
)


func WsEndpoint(w http.ResponseWriter, r *http.Request) {
	wsUpgrader.CheckOrigin = func(r *http.Request) bool {
		// check the request
		// make sure it is okey to access
		return true
	}

	wsConn, err := wsUpgrader.Upgrade(w, r, nil)
	
	if err != nil{
		fmt.Printf("can not upgrade: %s", err.Error())
	}
	defer wsConn.Close()

	// event loop

	for {
		var msg InitialMessage

		err := wsConn.ReadJSON(&msg)
		if err != nil {
			fmt.Printf("error reading JSON: %s", err.Error())
			break
		}

		fmt.Println("Initial Message Received: ", msg )
	}
}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/socket", WsEndpoint)
	log.Fatal(http.ListenAndServe(":8080", router))
}